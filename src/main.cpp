#include <QQmlApplicationEngine>
#include <QCommandLineParser>
#include <QIcon>
#include <QFileInfo>

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#else
#include <QApplication>
#endif

#include <MauiKit/mauiapp.h>
#include <KI18n/KLocalizedString>

#include "reel_version.h"

#include "models/recentmodel.h"

#define REEL_URI "org.slike.reel"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
    QCoreApplication::setAttribute(Qt::AA_DisableSessionManager, true);

#ifdef Q_OS_ANDROID
	QGuiApplication app(argc, argv);
	if (!MAUIAndroid::checkRunTimePermissions({"android.permission.WRITE_EXTERNAL_STORAGE"}))
		return -1;
#else
	QApplication app(argc, argv);
#endif

    app.setOrganizationName("Reel");
    app.setWindowIcon(QIcon(":/assets/reel.svg"));

    MauiApp::instance()->setIconName("qrc:/assets/reel.svg");
    MauiApp::instance()->setHandleAccounts(false);
    MauiApp::instance()->setEnableCSD(true);

    KLocalizedString::setApplicationDomain("reel");
    KAboutData about(QStringLiteral("reel"), i18n("Reel"), SLIKE_VERSION_STRING, i18n("Simple and convergent image editor."),
                     KAboutLicense::LGPL_V3, i18n("© 2019-%1 Slike Development Team", QString::number(QDate::currentDate().year())), QString(GIT_BRANCH) + "/" + QString(GIT_COMMIT_HASH));
    about.addAuthor(i18n("Camilo Higuita"), i18n("Developer"), QStringLiteral("milo.h@aol.com"));
    about.setHomepage("https://slike.org");
    about.setProductName("slike/reel");
    about.setBugAddress("https://invent.kde.org/camiloh/reel/-/issues");
    about.setOrganizationDomain(REEL_URI);
    about.setProgramLogo(app.windowIcon());

    KAboutData::setApplicationData(about);

	QQmlApplicationEngine engine;
	const QUrl url(QStringLiteral("qrc:/main.qml"));
	QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
	{
		if (!obj && url == objUrl)
			QCoreApplication::exit(-1);

	}, Qt::QueuedConnection);

    qmlRegisterType<RecentModel>(REEL_URI, 1, 0, "RecentFiles");

    engine.load(url);

#ifdef Q_OS_MACOS
//    MAUIMacOS::removeTitlebarFromWindow();
//    MauiApp::instance()->setEnableCSD(true); //for now index can not handle cloud accounts
#endif

	return app.exec();
}
