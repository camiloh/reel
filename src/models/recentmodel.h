#ifndef RECENTMODEL_H
#define RECENTMODEL_H

#include <mauilist.h>
#include <QObject>
#include <QUrl>

#include <MauiKit/mauilist.h>

class RecentModel : public MauiList
{
    Q_OBJECT
public:
    RecentModel();

    // QQmlParserStatus interface
public:
    void componentComplete() override final;

    const FMH::MODEL_LIST &items() const override final;

public slots:
    void insert(const QUrl &url);

private:
    FMH::MODEL_LIST m_list;
    QList<QUrl> m_urls;
    void setList();
};

#endif // RECENTMODEL_H
