#include "recentmodel.h"
#include <MauiKit/fmstatic.h>

RecentModel::RecentModel()
{

}

void RecentModel::componentComplete()
{
    this->setList();
}

const FMH::MODEL_LIST &RecentModel::items() const
{
    return m_list;
}

void RecentModel::insert(const QUrl &url)
{
    if(m_urls.contains(url))
        return;

    emit preItemAppended();
    m_list << FMH::getFileInfoModel(url);
    emit postItemAppended();

    m_urls << url;
    qDebug() << "Insert recents model" << m_urls;

    FMStatic::saveSettings("RECENT", QUrl::toStringList(m_urls), "FILES");
}

void RecentModel::setList()
{
    emit this->preListChanged();
    this->m_list.clear();
    this->m_urls.clear();

    m_urls= QUrl::fromStringList(FMStatic::loadSettings("RECENT", "FILES", {}).toStringList());

    qDebug() << "recents model" << m_urls;

   for(const auto &url : m_urls)
   {
       this->m_list << FMH::getFileInfoModel(url);
   }

   emit this->postListChanged();
}
