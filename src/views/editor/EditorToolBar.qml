import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.3

import org.kde.mauikit 1.2 as Maui
import org.kde.kirigami 2.8 as Kirigami

Item
{
    id: control
    implicitHeight: _layout.implicitHeight

    default property list<EditorToolBarItem> items

    Column
    {
        id: _layout
        width: parent.width


        Repeater
        {
            model: extractContents()
        }

        Maui.ToolBar
        {
            width: parent.width
            middleContent: Repeater
            {
                model: extractActions()

                ToolButton
                {
                    action: modelData
                }
            }
        }

    }


   function extractActions()
    {
        var actions = []
       for(var item of items)
       {
           actions.push(item.action)
       }

       return actions
    }

   function extractContents()
    {
        var contents = []
       for(var item of items)
       {
           actions.push(item.action)
       }

       return actions
    }

}
