import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.3

import org.kde.mauikit 1.2 as Maui
import org.kde.kirigami 2.8 as Kirigami

import org.slike.reel 1.0 as Reel

import "views/editor"

Maui.ApplicationWindow
{
    id: root

    altHeader: !isWide

    property alias dialog : _dialogLoader.item

    headBar.rightContent: ToolButton
    {
        icon.name: "folder-open"
        text: i18n("Open")
        onClicked:
        {

        }
    }

    Loader
    {
        id: _dialogLoader
    }

    Component
    {
        id: _fileDialogComponent

        Maui.FileDialog
        {

        }
    }

    headBar.middleContent : Label
    {
        Layout.fillWidth: true
        horizontalAlignment: Qt.AlignHCenter
        Layout.fillHeight: true
        verticalAlignment: Qt.AlignVCenter

        opacity: 0.5

        color: "#333"
        text: "Reel"

        font.bold: true
        font.weight: Font.Bold
        font.pointSize: 20

        font.family: "Lobster"
    }

    EditorView
    {
        id: _editorView
        anchors.fill: parent

        Maui.Holder
        {
            visible: !_editorView.ready
            emoji: "qrc:/assets/draw-watercolor.svg"
            emojiSize: Maui.Style.iconSizes.huge
            title: i18n("Start Editing")
            body: i18n("Open an image to start editing.")

            Action
            {
                text: i18n("Open")
                icon.name: "file-open"
                onTriggered:
                {
                    _dialogLoader.sourceComponent = _fileDialogComponent
                    dialog.settings.filterType = Maui.FMList.IMAGE
                    dialog.mode = dialog.modes.OPEN
                    dialog.callback = function(paths)
                    {
                        _editorView.url = paths[0]
                        _recentFiles.insert(paths[0])
                    }
                    dialog.open()
                }
            }


            content: [

                Maui.GridView
                {
                    id: _recentGridView
                    width: parent.width
                    height: Math.min(500, contentHeight + Maui.Style.space.big)
                    itemSize: Math.min(width/2, 150)

                    model: Maui.BaseModel
                    {
                        list: Reel.RecentFiles
                        {
                            id: _recentFiles
                        }
                    }

                    delegate:Item
                    {
                        width: _recentGridView.itemSize
                        height: _recentGridView.cellHeight

                        Maui.GridBrowserDelegate
                        {
                            anchors.fill: parent
                            anchors.margins: Maui.Style.space.small

                            iconSizeHint: height
                            label1.text: model.label
                            template.labelsVisible: false
                            iconSource: model.icon
                            imageSource: model.thumbnail

                            onClicked:
                            {
                                _editorView.url = model.url
                            }
                        }
                    }
                }]
        }
    }

}
